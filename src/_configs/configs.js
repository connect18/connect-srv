const configs = {
  env: process.env.NODE_ENV,
  isDev: process.env.NODE_ENV !== 'production',
  isProd: process.env.NODE_ENV === 'production',

  appUrl: 'https://sosequipements.fr',
  apiUrl: 'https://evening-taiga-61276.herokuapp.com',

  sendGrid: {
    apiKey: process.env.SENDGRID_API_KEY,
    sender: {
      email: 'contact@sosequipements.fr',
      name: 'SOS Équipements',
    },
    templates: {
      afterSubmitOffer: 'd-d83e3f3ea3ae456c91a57b2ed6b26bb1',
      afterSubmitRequest: 'd-d83e3f3ea3ae456c91a57b2ed6b26bb1',
    },
  },

  airtable: {
    apiKey: process.env.AIRTABLE_API_KEY,
    requests: {
      baseId: 'appfYJB5jH9fYYiKG',
      tableId: 'Table%201',
      appPath: '/requests',
      filterName: 'requests',
      resourcesFieldId: 'Votre besoin',
    },
    offers: {
      baseId: 'app73vh4xwPhn2Fho',
      tableId: 'Table%201',
      appPath: '/offers',
      filterName: 'offers',
      resourcesFieldId: 'Vous pouvez offrir',
    },
    bases: [
      {id: 'appfYJB5jH9fYYiKG', rights: ['read', 'write']},
      {id: 'app73vh4xwPhn2Fho', rights: ['read', 'write']},
    ],
  },
};

module.exports = configs;
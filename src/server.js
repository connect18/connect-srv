require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const configs = require('./_configs/configs');
const airtable = require('./airtable/airtable');

const app = express();
const port = process.env.PORT || 4000;

app.use(cors());
app.use(morgan(configs.isDev ? 'dev' : 'common'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/v0', airtable);
app.get('*', (req, res) => res.send('OK'));

app.listen(port);

console.info('[INFO]', 'Connect API server started on :', port);

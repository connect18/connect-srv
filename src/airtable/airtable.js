const express = require('express');
const asyncMiddleware = require('../utils/asyncMiddleware');
const axios = require('axios');
const cron = require('cron');
const sgMail = require('@sendgrid/mail');
const configs = require('../_configs/configs');

sgMail.setApiKey(configs.sendGrid.apiKey);
const router = express.Router();
const airTable = axios.create({
  baseURL: 'https://api.airtable.com/v0/',
  headers: {'Authorization': `Bearer ${configs.airtable.apiKey}`},
});

// Delete a record
router.get(
  '/delete/:baseId/:tableName',
  asyncMiddleware(async function (req, res) {
    try {
      const base = configs.airtable.bases.find(base => base.id === req.params.baseId);
      if (!base || !base.rights.includes('write')) {
        res.status(403);
        res.json({
          error: {
            type: 'FORBIDDEN_BASE',
            message: 'You dont have the right to access this base !',
          },
        });
        return;
      }

      const queryString = req.url.substr(req.url.indexOf('?') + 1);
      await airTable({
        method: 'delete',
        url: `/${req.params.baseId}/${req.params.tableName}?${queryString}`,
      });
      res.redirect(configs.appUrl + '?deleted=true');
    } catch (error) {
      const {response} = error;
      if (response) {
        res.status(response.status);
        res.json(response.data);
      } else {
        res.status(500);
        res.json({
          error: {
            type: 'AIRTABLE_API_LINK',
            message: 'Something went wrong while rerouting the request, please try again.',
          },
        });
      }
    }
  }),
);

// get records
router.get('/:baseId/:tableName', asyncMiddleware(async function (req, res, next) {
  try {
    const base = configs.airtable.bases.find(base => base.id === req.params.baseId);
    if (!base || !base.rights.includes('read')) {
      res.status(403);
      res.json({
        error: {
          type: 'FORBIDDEN_BASE',
          message: 'You dont have the right to access this base !',
        },
      });
      return;
    }

    const queryString = req.url.substr(req.url.indexOf('?') + 1);
    const response = await airTable({
      method: 'get',
      url: `/${req.params.baseId}/${req.params.tableName}?${queryString}`,
    });
    res.status(response.status);
    res.json(response.data);
  } catch (error) {
    const {response} = error;
    if (response) {
      res.status(response.status);
      res.json(response.data);
    } else {
      res.status(500);
      res.json({
        error: {
          type: 'AIRTABLE_API_LINK',
          message: 'Something went wrong while rerouting the request, please try again.',
        },
      });
    }
  }
}));

// cron to send emails
if (process.env.NODE_APP_INSTANCE === undefined || process.env.NODE_APP_INSTANCE === '0') {
  new cron.CronJob({
    cronTime: '00 */1 * * * *', // every 1min
    onTick: async () => {
      console.info('[INFO]', 'CRON - Send notifications');
      try {
        sendNotifs(
          configs.airtable.offers.baseId, configs.airtable.offers.tableId, configs.airtable.requests.appPath,
          configs.airtable.requests.filterName, configs.airtable.offers.resourcesFieldId,
          configs.sendGrid.templates.afterSubmitOffer,
        );
        sendNotifs(
          configs.airtable.requests.baseId, configs.airtable.requests.tableId, configs.airtable.offers.appPath,
          configs.airtable.offers.filterName, configs.airtable.requests.resourcesFieldId,
          configs.sendGrid.templates.afterSubmitOffer,
        );
      } catch (err) {
        console.error(err);
      }
    },
    start: false,
    timeZone: 'Europe/Paris',
  }).start();
}

async function sendNotifs(baseId, tableId, appPath, offerId, offerField, templateId) {
  const response = await airTable({
    method: 'get',
    url: `/${baseId}/${tableId}?filterByFormula=NOT({$notified} = TRUE())`,
  });
  const hasBeenNotified = [];

  for (let i = 0; i < response.data.records.length; i++) {
    try {
      const record = response.data.records[i];
      if (!record.fields.Email) continue;
      const url = configs.appUrl + appPath + '?departement=' + encodeURIComponent(record.fields['Département']) +
        '&' + offerId + '=' + encodeURIComponent(record.fields[offerField].join(','));
      const deleteUrl = configs.apiUrl + '/v0/delete/' + baseId + '/' + tableId + '?records[]=' + record.id;
      const msg = {
        to: record.fields.Email,
        from: configs.sendGrid.sender,
        template_id: templateId,
        dynamic_template_data: {
          app_url: url,
          delete_url: deleteUrl,
        },
      };
      sgMail.send(msg);
      hasBeenNotified.push(record.id);
    } catch (err) {
      console.error('[ERROR]', err);
    }
  }

  if (hasBeenNotified.length > 0) {
    await airTable({
      method: 'patch',
      url: `/${baseId}/${tableId}`,
      data: {
        records: hasBeenNotified.map((recordId) => ({
          id: recordId,
          fields: {
            $notified: true,
          },
        })),
      },
    });
  }
}

module.exports = router;
